<?php

declare(strict_types=1);

namespace Categories\Handler;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Helper\ServerUrlHelper;

/**
 * Class CategoriesDeleteHandlerFactory
 * @package Categories\Handler
 */
class CategoriesDeleteHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return CategoriesDeleteHandler
     */
    public function __invoke(ContainerInterface $container) : CategoriesDeleteHandler
    {
        $entityManager = $container->get(EntityManager::class);

        $urlHelper = $container->get(ServerUrlHelper::class);

        return new CategoriesDeleteHandler($entityManager, $urlHelper);
    }
}
