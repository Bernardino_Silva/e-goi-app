<?php

declare(strict_types=1);

namespace Categories\Handler;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Helper\ServerUrlHelper;

/**
 * Class CategoriesCreateHandlerFactory
 * @package Categories\Handler
 */
class CategoriesCreateHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return CategoriesCreateHandler
     */
    public function __invoke(ContainerInterface $container) : CategoriesCreateHandler
    {
        $entityManager = $container->get(EntityManager::class);

        $urlHelper = $container->get(ServerUrlHelper::class);

        return new CategoriesCreateHandler($entityManager, $urlHelper);
    }
}
