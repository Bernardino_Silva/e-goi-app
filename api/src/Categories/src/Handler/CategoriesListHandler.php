<?php

declare(strict_types=1);

namespace Categories\Handler;

use Categories\Model\Category;
use Categories\Model\CategoriesCollection;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

/**
 * Class CategoriesReadHandler
 * @package Categories\Handler
 */
class CategoriesListHandler implements RequestHandlerInterface
{
    protected $entityManager;
    protected $pageCount;
    protected $responseFactory;
    protected $resourceGenerator;

    /**
     * CategoriesReadHandler constructor.
     * @param EntityManager $entityManager
     * @param $pageCount
     * @param HalResponseFactory $responseFactory
     * @param ResourceGenerator $resourceGenerator
     */
    public function __construct(EntityManager $entityManager, $pageCount, HalResponseFactory $responseFactory, ResourceGenerator $resourceGenerator
    ) {
        $this->entityManager = $entityManager;
        $this->pageCount = $pageCount;
        $this->responseFactory = $responseFactory;
        $this->resourceGenerator = $resourceGenerator;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $repository = $this->entityManager->getRepository(Category::class);

        $query = $repository
            ->createQueryBuilder('a')
            ->addOrderBy('a.name', 'asc')
            ->setMaxResults($this->pageCount)
            ->getQuery();

        $paginator = new CategoriesCollection($query);
        $resource  = $this->resourceGenerator->fromObject($paginator, $request);
        return $this->responseFactory->createResponse($request, $resource);
    }
}
