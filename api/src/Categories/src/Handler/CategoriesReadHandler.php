<?php

declare(strict_types=1);

namespace Categories\Handler;

use Categories\Model\Category;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use RuntimeException;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;
use Zend\responseDetails\Exception\CommonresponseDetailsExceptionTrait;
use Zend\responseDetails\Exception\responseDetailsExceptionInterface;

class CategoriesReadHandler implements RequestHandlerInterface
{
    protected $entityManager;
    protected $halResponseFactory;
    protected $resourceGenerator;

    /**
     * CategoriesViewHandler constructor.
     * @param EntityManager $entityManager
     * @param HalResponseFactory $halResponseFactory
     * @param ResourceGenerator $resourceGenerator
     */
    public function __construct(EntityManager $entityManager, HalResponseFactory $halResponseFactory, ResourceGenerator $resourceGenerator) {
        $this->entityManager = $entityManager;
        $this->halResponseFactory = $halResponseFactory;
        $this->resourceGenerator = $resourceGenerator;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $id = $request->getAttribute('id', null);
        $repository = $this->entityManager->getRepository(Category::class);
        $entity = $repository->find($id);

        if (empty($entity)) {

            $response = new class ($id) extends RuntimeException implements responseDetailsExceptionInterface {
                use CommonresponseDetailsExceptionTrait;

                public function __construct(?string $id)
                {
                    $this->detail = sprintf('The category with ID "%s" not exist', (string) $id);
                    $this->status = 404;
                    $this->title  = 'Error.';
                    $this->type = 'error.category.not-found';
                    parent::__construct($this->detail, $this->status);
                }
            };

            throw $response;
        }

        $resource = $this->resourceGenerator->fromObject($entity, $request);
        return $this->halResponseFactory->createResponse($request, $resource);
    }
}