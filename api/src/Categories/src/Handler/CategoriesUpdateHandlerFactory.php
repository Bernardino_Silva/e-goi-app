<?php

declare(strict_types=1);

namespace Categories\Handler;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Helper\ServerUrlHelper;

/**
 * Class CategoriesUpdateHandlerFactory
 * @package Categories\Handler
 */
class CategoriesUpdateHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return CategoriesUpdateHandler
     */
    public function __invoke(ContainerInterface $container) : CategoriesUpdateHandler
    {
        $entityManager = $container->get(EntityManager::class);

        $urlHelper = $container->get(ServerUrlHelper::class);

        return new CategoriesUpdateHandler($entityManager, $urlHelper);
    }
}
