<?php

declare(strict_types=1);

namespace Categories\Handler;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

/**
 * Class CategoriesReadHandlerFactory
 * @package Categories\Handler
 */
class CategoriesListHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return CategoriesReadHandler
     */
    public function __invoke(ContainerInterface $container) : CategoriesListHandler
    {
        $entityManager = $container->get(EntityManager::class);

        $resourceGenerator = $container->get(ResourceGenerator::class);
        $responseFactory = $container->get(HalResponseFactory::class);

        return new CategoriesListHandler($entityManager, $container->get('config')['page_size'], $responseFactory, $resourceGenerator);
    }
}
