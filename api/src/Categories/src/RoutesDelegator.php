<?php 

namespace Categories;

use Categories\Handler;
use Psr\Container\ContainerInterface;
use Zend\ProblemDetails\ProblemDetailsMiddleware;
use Zend\Expressive\Application;

class RoutesDelegator {

    /**
     * @param ContainerInterface $container
     * @param string $serviceName Name of the service being created.
     * @param callable $callback Creates and returns the service.
     * @return Application
     */
    public function __invoke(ContainerInterface $container, $serviceName, callable $callback) {
        $app = $callback();

        $app->post('/categories[/]', Handler\CategoriesCreateHandler::class, 'categories.create');
        $app->get('/categories/[?page={page:\d+}]', Handler\CategoriesListHandler::class, 'categories.list');
       
        $app->get('/categories/{id:[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}}[/]', [
            ProblemDetailsMiddleware::class,
            Handler\CategoriesReadHandler::class,
        ], 'categories.read');

        $app->put('/categories/{id:[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}}[/]', Handler\CategoriesUpdateHandler::class, 'categories.update');

        $app->delete('/categories/{id:[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}}[/]', Handler\CategoriesDeleteHandler::class, 'categories.delete');

        return $app;
    }
}