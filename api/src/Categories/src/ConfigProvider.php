<?php

declare(strict_types=1);

namespace Categories;

use Categories\Model\Category;
use Categories\Model\CategoriesCollection;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Expressive\Application;
use Zend\Expressive\Hal\Metadata\MetadataMap;
use Zend\Expressive\Hal\Metadata\RouteBasedCollectionMetadata;
use Zend\Expressive\Hal\Metadata\RouteBasedResourceMetadata;
use Zend\Hydrator\ReflectionHydrator;

/**
 * The configuration provider for the Categories module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke() : array {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
            'doctrine'     => $this->getDoctrineEntities(),
            MetadataMap::class => $this->getHalMetadataMap(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array {
        return [
            'delegators' => [
                Application::class => [
                    RoutesDelegator::class,
                ],
            ],
            'invokables' => [
            ],
            'factories'  => [
                Handler\CategoriesListHandler::class => Handler\CategoriesListHandlerFactory::class,
                Handler\CategoriesCreateHandler::class => Handler\CategoriesCreateHandlerFactory::class,
                Handler\CategoriesReadHandler::class => Handler\CategoriesReadHandlerFactory::class,
                Handler\CategoriesUpdateHandler::class => Handler\CategoriesUpdateHandlerFactory::class,
                Handler\CategoriesDeleteHandler::class => Handler\CategoriesDeleteHandlerFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates() : array
    {
        return [
            'paths' => [
                'categories'    => [__DIR__ . '/../templates/'],
            ],
        ];
    }
    public function getDoctrineEntities() : array
    {
        return [
            'driver' => [
                'orm_default' => [
                    'class' => MappingDriverChain::class,
                    'drivers' => [
                        'Categories\Model' => 'category_entity',
                    ],
                ],
                'category_entity' => [
                    'class' => AnnotationDriver::class,
                    'cache' => 'array',
                    'paths' => [__DIR__ . '/Model'],
                ],
            ],
        ];
    }

    public function getHalMetadataMap()
    {
        return [
            [
                '__class__' => RouteBasedResourceMetadata::class,
                'resource_class' => Category::class,
                'route' => 'categories.read',
                'extractor' => ReflectionHydrator::class,
            ],
            [
                '__class__' => RouteBasedCollectionMetadata::class,
                'collection_class' => CategoriesCollection::class,
                'collection_relation' => 'category',
                'route' => 'categories.list',
            ],
        ];
    }
}
