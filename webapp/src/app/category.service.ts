import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private baseUrl = 'http://34.70.232.144/api/categories/';

  constructor(private http: HttpClient) { }

  getCategory(id: string): Observable<any> {
    return this.http.get(`${this.baseUrl}${id}`);
  }

  createCategory(category: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, category);
  }

  updateCategory(id: string, category: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}${id}`, category);
  }

  deleteCategory(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}${id}`, { responseType: 'text' });
  }

  getCategoriesList(): Observable<any> {
    return this.http.get(this.baseUrl);
  }
}
