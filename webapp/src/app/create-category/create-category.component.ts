import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { Category } from '../category';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent implements OnInit {
  category: Category = new Category();
  title = "";
  submitted = false;
  json = null;

  constructor(private categoryService: CategoryService, private router: Router) { }

  ngOnInit() {
  }

  save() {
    this.categoryService.createCategory(this.json).subscribe(
      data => console.log(data), error => console.log(error)
    );
    this.category = new Category();
    this.goTo();
  }

  onSubmit() {
    this.submitted = true;
    this.json = {
      "Request": {
          "Categories": {
              "name": this.category.name,
              "description": this.category.description
          }
      }
    };
    this.save();    
  }

  goTo() {
    this.router.navigate(['/categories']);
  }

}
