import { Observable } from "rxjs";
import { CategoryService } from "../category.service";
import { Category } from "../category";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {
  categories = new Array();
  page:number = 1;
  page_count = 1;
  total_items = 0;
  title = "";

  constructor(private categoryService: CategoryService, private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.categories = new Array();
    this.categoryService.getCategoriesList().subscribe((v) => {
      console.log(v);
      if (v._total_items > 0) {
        let tmp = v._embedded.category;

        for (let i = 0; i < v._total_items; ++i) {
          let cat = tmp[i];
          cat.createdAt.date = moment(cat.createdAt.date).format('DD/MM/YYYY HH:mm');
          cat.updateAt.date = moment(cat.updateAt.date).format('DD/MM/YYYY HH:mm');
          this.categories.push(tmp[i]);
        }
      }
    });
  }

  deleteCategory(id: string) {
    this.categoryService.deleteCategory(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  categoryDetails(id: string){
    this.router.navigate(['details', id]);
  }

}
