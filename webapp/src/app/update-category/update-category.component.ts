import { Component, OnInit } from '@angular/core';
import { Category } from '../category';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../category.service';
import * as moment from 'moment';

@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.css']
})
export class UpdateCategoryComponent implements OnInit {
  id: string;
  category: Category;
  json = null;
  title = "";
  submitted = false;

  constructor(private route: ActivatedRoute,private router: Router, private categoryService: CategoryService) { }

  ngOnInit() {
    this.category = new Category();
    this.id = this.route.snapshot.params['id'];
    
    this.categoryService.getCategory(this.id)
      .subscribe(data => {
        this.category = data;
      }, error => console.log(error));
  }
  
  updateCategory() {
    this.submitted = true;
    this.json = {
      "Request": {
          "Categories": {
              "name": this.category.name,
              "description": this.category.description
          }
      }
    };
    this.categoryService.updateCategory(this.id, this.json)
      .subscribe(data => console.log(data), error => console.log(error));
    this.category = new Category();
    this.goTo();

  }

  onSubmit() {
    this.updateCategory();    
  }

  goTo() {
    this.router.navigate(['/categories']);
  }

  categoryDetails(id: string){
    this.router.navigate(['details', id]);
  }
}
