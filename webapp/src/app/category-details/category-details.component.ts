import { Category } from '../category';
import { CategoryService } from '../category.service';
import { CategoryListComponent } from '../category-list/category-list.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';


@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.css']
})
export class CategoryDetailsComponent implements OnInit {
  id: string;
  category: Category;
  title = "";

  constructor(private route: ActivatedRoute,private router: Router,
    private categoryService: CategoryService) { }

  ngOnInit() {
    this.category = new Category();
    this.id = this.route.snapshot.params['id'];
  
    this.categoryService.getCategory(this.id)
      .subscribe(data => {
        console.log(data)
        this.category = data;
        this.category.createdAt = moment(data.createdAt.date).format('DD/MM/YYYY HH:mm');
        this.category.updatedAt = moment(data.updateAt.date).format('DD/MM/YYYY HH:mm');
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['categories']);
  }

  updateCategory(id: string){
    this.router.navigate(['update', id]);
  }
}
